package leroymerlin;

import leroymerlin.upload.xml.service.UploadXml;
import leroymerlin.upload.xml.service.impl.UploadXmlImpl;

public class Application {
    private static final String fileSource = "src/main/resources/ABCDRUMM_camt.053.001.02.RU2018.01(FCY).xml";
    public static void main(String[] args) {
        UploadXml uploadXml = new UploadXmlImpl();
        uploadXml.load(fileSource);

    }
}
