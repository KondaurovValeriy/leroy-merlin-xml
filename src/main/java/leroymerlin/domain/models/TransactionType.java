package leroymerlin.domain.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "transaction_type")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TransactionType {
    @Id
    private Long id;

    @Column(name = "code")
    private String code;

    @Column(name = "family")
    private String family;
}
