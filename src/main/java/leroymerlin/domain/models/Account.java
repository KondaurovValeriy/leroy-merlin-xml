package leroymerlin.domain.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Table(name = "account")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Account {
    @Id
    private Long id;

    @Column(name = "name")
    private String number;

    @Column(name = "client_id")
    @ManyToOne(targetEntity = Client.class)
    private Client client;

    @Column(name = "bank_id")
    @ManyToOne(targetEntity = Bank.class)
    private Bank bank;

}
