package leroymerlin.domain.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "bank")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Bank {
    @Id
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "swift_number")
    private String swiftNumber;

    @Column(name = "country")
    private String country;

}
