package leroymerlin.domain.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "transaction")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Transaction {
    @Id
    private Long id;

    @Column(name = "currency")
    private String currency;

    @Column(name = "amount")
    private Double amount;

    @Column(name = "status")
    private String status;

    @Column(name = "date_val")
    private Date dateVal;

    @Column(name = "date_book")
    private Date dateBook;

    @Column(name = "purpose")
    private String purpose;

    @Column(name = "type_id")
    @ManyToOne(targetEntity = TransactionType.class)
    private TransactionType typeTrans;

    @Column(name = "sender_id")
    @ManyToOne(targetEntity = Account.class)
    private Account accountSender;

    @Column(name = "recipient_id")
    @ManyToOne(targetEntity = Account.class)
    private Account accountRecipient;

}
