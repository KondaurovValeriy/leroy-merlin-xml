package leroymerlin.domain.service.impl;

import leroymerlin.domain.exception.BankException;
import leroymerlin.domain.models.Client;
import leroymerlin.domain.repository.ClientRepository;
import leroymerlin.domain.service.ClientService;
import leroymerlin.domain.service.TransformationService;
import leroymerlin.upload.xml.models.ClientXml;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClientServiceImpl implements ClientService {
    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private TransformationService transformationService;

    @Override
    public Client getByInn(String inn) {
        if (inn != null) {
            Client client = clientRepository.findByInn(inn);
            if (client != null) {
                return client;
            }
        }
        return null;
    }

    @Override
    public Client getByName(String name) {
        if (name != null) {
            Client client = clientRepository.findByName(name);
            if (client != null) {
                return client;
            }
        }
        return null;
    }

    @Override
    public Client save(ClientXml clientXml) {
        if (clientXml.getTxId() != null) {
            if (!clientRepository.existsByInn(clientXml.getTxId())) {
                Client client = transformationService.getClientFromXml(clientXml);
                client = clientRepository.save(client);
                return client;
            } else {
                throw new BankException("Bank with swift code " + clientXml.getTxId() + "exist in DB");
            }
        } throw new BankException("Swift code bank for saving is not set");
    }

    @Override
    public Client findOrSave(ClientXml clientXml) {
        if (clientXml != null) {
            Client client = this.getByInn(clientXml.getTxId());
            if (client == null) {
                client = this.save(clientXml);
            }
            return client;
        }
        return null;
    }
}
