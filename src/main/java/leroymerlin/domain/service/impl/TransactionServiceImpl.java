package leroymerlin.domain.service.impl;

import leroymerlin.domain.models.*;
import leroymerlin.domain.repository.TransactionRepository;
import leroymerlin.domain.service.*;
import leroymerlin.upload.xml.models.TransactionXml;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;

import java.util.List;

public class TransactionServiceImpl implements TransactionService {

    @Autowired
    private ClientService clientService;

    @Autowired
    private BankService bankService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private TransactionTypeService transactionTypeService;

    @Autowired
    private TransformationService transformationService;

    @Autowired
    private TransactionRepository transactionRepository;

    @Override
    public TransactionXml save(TransactionXml transactionXml) {
        if (transactionXml != null) {
                // Data Sender
                Client clientSender = clientService.findOrSave(transactionXml.getClientSender());
                transactionXml.getClientSender().setId(clientSender.getId());

                Bank bankSender = bankService.findOrSave(transactionXml.getBankSender());
                transactionXml.getBankSender().setId(bankSender.getId());

                Account accountSender =
                        accountService.findOrSave(transactionXml.getAccountSender(), clientSender, bankSender);
                transactionXml.getAccountSender().setId(accountSender.getId());

                // Data Recipient
                Client clientRecipient = clientService.findOrSave(transactionXml.getClientRecipient());
                transactionXml.getClientRecipient().setId(clientRecipient.getId());

                Bank bankRecipient = bankService.findOrSave(transactionXml.getBankRecipient());
                transactionXml.getBankRecipient().setId(bankRecipient.getId());

                Account accountRecipient =
                        accountService.findOrSave(transactionXml.getAccountRecipient(), clientRecipient, bankRecipient);
                transactionXml.getAccountRecipient().setId(accountRecipient.getId());

                TransactionType transactionType
                        = transactionTypeService.findOrSave(transactionXml.getTransactionTypeXml());
                transactionXml.getTransactionTypeXml().setId(transactionType.getId());

                Transaction transaction = transformationService.getTransactionFromXml(transactionXml.getTransactionDetailsXml(),
                        accountSender, accountRecipient, transactionType);

                if (!transactionRepository.exists(Example.of(transaction))) {
                    transaction = transactionRepository.save(transaction);
                    transactionXml.setId(transaction.getId());
                    return transactionXml;
                }

        }
        return null;
    }

    @Override
    public List<TransactionXml> saveAll(List<TransactionXml> transactionXmlList) {
        for (TransactionXml t: transactionXmlList) {
            t = this.save(t);
        }
        return transactionXmlList;
    }
}
