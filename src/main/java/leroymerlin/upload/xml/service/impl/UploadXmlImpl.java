package leroymerlin.upload.xml.service.impl;

import leroymerlin.upload.xml.models.TransactionXml;
import leroymerlin.upload.xml.service.TransformationService;
import leroymerlin.upload.xml.service.UploadXml;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class UploadXmlImpl implements UploadXml {


    private String fileProperties = "src/main/resources/swift___.properties";
    private String[] currency = {"(RUB)", "(FCY)"};
    private TransformationService transformationService;

    public UploadXmlImpl() {
        this.transformationService = new TransformationServiceImpl();
    }

    public void load (String fileSource)  {
        for (String c:currency) {
            if (fileSource.contains(c))
                fileProperties= fileProperties.replace("___", c);
        }

        Map<String, String> fieldsTransaction = getPropertiesToMap(fileProperties);

        File file = new File(fileSource);
        SAXBuilder saxBuilder = new SAXBuilder();
        Document build = null;
        try {
            build = saxBuilder.build(file);
        } catch (JDOMException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Element rootElement = build.getRootElement();
        List<Element> entityTransactions = getByName(fieldsTransaction.get("root"), rootElement);
        fieldsTransaction.remove("root");

        Map<String, String> valueTransaction = new HashMap();
        List<TransactionXml> transactionXmlList = new ArrayList<>();
        for(Element e: entityTransactions) {
            valueTransaction.clear();
            for (Map.Entry<String, String> entry:fieldsTransaction.entrySet()) {
                String value = entry.getValue();
                if (value.startsWith("!")) {
                    String nameKey = value.substring(1, value.indexOf('/'));
                    String nameAttributes = value.substring(value.indexOf('/') + 1);
                    valueTransaction.put(entry.getKey(),
                            getByName(fieldsTransaction.get(nameKey), e).get(0).getAttribute(nameAttributes).getValue());
               } else {
                   valueTransaction.put(entry.getKey(), getValueByName(value, e));
               }
            }
            transactionXmlList.add(transformationService.getTransactionEntityFromMap(valueTransaction));
        }
        for (TransactionXml transactionXml : transactionXmlList) {
            System.out.println(transactionXml.toString());
        }
    }



    private  String getValueByName(String name, Element root) {
        List<Element> elements = getByName(name, root);
        if (!elements.isEmpty()) {
            return elements.get(0).getValue();
        }
        return null;
    }


    private List<Element> getByName(String name, Element root) {
        String[] names = name.split("/");
        List<Element> currentElements = null;
        for (String searchName : names) {
            if (currentElements == null) {
                currentElements = root.getChildren().stream().filter(c -> c.getName().equals(searchName)).collect(Collectors.toList());
            } else {
                List<Element> newCurrentElements = new ArrayList<>();
                for (Element e : currentElements) {
                    List<Element> collect = e.getChildren().stream().filter(c -> c.getName().equals(searchName)).collect(Collectors.toList());
                    if (!collect.isEmpty()) newCurrentElements.addAll(collect);
                }
                currentElements = newCurrentElements;
            }
        }
        return currentElements;
    }

    private Map<String, String> getPropertiesToMap(String fileProperties) {
        Properties property = new Properties();
        try {
            property.load(new FileInputStream(fileProperties));
        } catch (IOException e) {
            e.printStackTrace();
        }

        Map<String, String> map = new HashMap();
        for (Map.Entry<Object, Object> entry:property.entrySet()) {
            map.put((String) entry.getKey(), (String) entry.getValue());
        }
        return map;
    }

}
